package com.AppiumDemo;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class appium {
    @Test
    public void Test() throws MalformedURLException {

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("device", "emulator-5554");
        caps.setCapability("os_version", "10.0");
        caps.setCapability("platformName", "Android");
        caps.setCapability("automationName", "Appium");
        caps.setCapability("app", "D:\\Quiz-app.apk");
        URL url = new URL("http://127.0.0.1:4723/wd/hub");

        // Start Testing Process
        // New Android Driver
        AndroidDriver driver = new AndroidDriver(url, caps);
        // Waiting
        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
        // Find Element By ID
        driver.findElementById("com.example.vikasojha.quizbee:id/editName").click();
        driver.findElementById("com.example.vikasojha.quizbee:id/editName").sendKeys("Ibrahim");
        driver.hideKeyboard();

        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
        driver.findElementById("com.example.vikasojha.quizbee:id/button").click();

        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
        driver.findElementById("com.example.vikasojha.quizbee:id/radioButton2").click();

        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
        driver.findElement(MobileBy.AndroidUIAutomator("text(\"NEXT QUESTION\")")).click();

        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
        driver.findElement(MobileBy.AndroidUIAutomator("text(\"QUIT\")")).click();


    }

}